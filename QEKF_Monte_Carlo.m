close all;

% Determine where your m-file's folder is.
folder = fileparts(which(mfilename)); 
% Add that folder plus all subfolders to the path.
addpath(genpath(folder));

% Number of Monte Carlo simulations
numSimulations = 100;

% Generate truth signal to determine the duration and dt (assuming the first call returns consistent values)
[~, ~, ~, ~, ~, duration, dt] = generateMeasurements();

% Initialize storage for results
posErrors = zeros(numSimulations, duration/dt);
velErrors = zeros(numSimulations, duration/dt);
attitudeErrors = zeros(numSimulations, 3, duration/dt);

for sim = 1:numSimulations
    % Generate truth signal and noisy measurements with different noise each iteration
    [f, w, v, p, Rcart, duration, dt] = generateMeasurements();

    % Run QEKF filter
    [R, v_, p_] = QEKF_run(f, w, p, Rcart, duration, dt);

    % Convert to Euler angles for visualization
    Rcart_euler = zeros(3, duration/dt);  % Initialize an array to hold the Euler angles for truth
    Rcart_ = zeros(3, duration/dt);  % Initialize an array to hold the Euler angles for estimation
    for i = 1:duration/dt
        Rcart_euler(:, i) = rotm2eul(Rcart(:, :, i), 'ZYX');
        Rcart_(:, i) = rotm2eul(R(:, :, i), 'ZYX');
    end
    
    % Calculate errors
    posErrors(sim, :) = p(1, :) - p_(1, :);
    velErrors(sim, :) = v(1, :) - v_(1, :);
    attitudeErrors(sim, :, :) = Rcart_euler - Rcart_;
end

% Compute statistics
meanPosError = mean(posErrors, 1);
varPosError = var(posErrors, 1);

meanVelError = mean(velErrors, 1);
varVelError = var(velErrors, 1);

meanAttitudeError = squeeze(mean(attitudeErrors, 1));
varAttitudeError = squeeze(var(attitudeErrors, 1));

% Plotting results
figure(1);
rows = 3;
columns = 3;
i = 0;

% Plot mean position error
i = i + 1;
subplot(rows, columns, i);
plot(meanPosError, 'linewidth', 2)
hold on; grid on
ylabel('$x$', 'fontsize', 14, 'Interpreter', 'latex')
legend({'Mean Position Error'}, 'location', 'Best')
axis([0 length(f) -50 50]);
title('Mean Position Error')

% Plot variance of position error
i = i + 1;
subplot(rows, columns, i);
plot(varPosError, 'linewidth', 2)
hold on; grid on;
ylabel('$x$', 'fontsize', 14, 'Interpreter', 'latex')
legend({'Variance of Position Error'}, 'location', 'Best')
axis([0 length(f) 0 100]);
title('Variance of Position Error')

% Plot mean velocity error
i = i + 1;
subplot(rows, columns, i);
plot(meanVelError, 'linewidth', 2)
hold on; grid on
ylabel('$x$', 'fontsize', 14, 'Interpreter', 'latex')
legend({'Mean Velocity Error'}, 'location', 'Best')
axis([0 length(f) -50 50]);
title('Mean Velocity Error')

% Plot variance of velocity error
i = i + 1;
subplot(rows, columns, i);
plot(varVelError, 'linewidth', 2)
hold on; grid on;
ylabel('$x$', 'fontsize', 14, 'Interpreter', 'latex')
legend({'Variance of Velocity Error'}, 'location', 'Best')
axis([0 length(f) 0 100]);
title('Variance of Velocity Error')

% Plot mean attitude error
i = i + 1;
subplot(rows, columns, i);
plot(meanAttitudeError(1, :), 'linewidth', 2)
hold on; grid on
ylabel('$x$', 'fontsize', 14, 'Interpreter', 'latex')
legend({'Mean Attitude Error (x)'}, 'location', 'Best')
axis([0 length(f) -4 4]);
title('Mean Attitude Error (euler angles "x")')

i = i + 1;
subplot(rows, columns, i);
plot(meanAttitudeError(2, :), 'linewidth', 2)
hold on; grid on
ylabel('$x$', 'fontsize', 14, 'Interpreter', 'latex')
legend({'Mean Attitude Error (y)'}, 'location', 'Best')
axis([0 length(f) -4 4]);
title('Mean Attitude Error (euler angles "y")')

i = i + 1;
subplot(rows, columns, i);
plot(meanAttitudeError(3, :), 'linewidth', 2)
hold on; grid on
ylabel('$x$', 'fontsize', 14, 'Interpreter', 'latex')
legend({'Mean Attitude Error (z)'}, 'location', 'Best')
axis([0 length(f) -4 4]);
title('Mean Attitude Error (euler angles "z")')

% Plot variance of attitude error
i = i + 1;
subplot(rows, columns, i);
plot(varAttitudeError(1, :), 'linewidth', 2)
hold on; grid on
ylabel('$x$', 'fontsize', 14, 'Interpreter', 'latex')
legend({'Variance of Attitude Error (x)'}, 'location', 'Best')
axis([0 length(f) 0 16]);
title('Variance of Attitude Error (euler angles "x")')

i = i + 1;
subplot(rows, columns, i);
plot(varAttitudeError(2, :), 'linewidth', 2)
hold on; grid on
ylabel('$x$', 'fontsize', 14, 'Interpreter', 'latex')
legend({'Variance of Attitude Error (y)'}, 'location', 'Best')
axis([0 length(f) 0 16]);
title('Variance of Attitude Error (euler angles "y")')

i = i + 1;
subplot(rows, columns, i);
plot(varAttitudeError(3, :), 'linewidth', 2)
hold on; grid on
ylabel('$x$', 'fontsize', 14, 'Interpreter', 'latex')
legend({'Variance of Attitude Error (z)'}, 'location', 'Best')
axis([0 length(f) 0 16]);
title('Variance of Attitude Error (euler angles "z")')
