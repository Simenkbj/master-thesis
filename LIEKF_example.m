close all;

% Determine where your m-file's folder is.
folder = fileparts(which(mfilename)); 
% Add that folder plus all subfolders to the path.
addpath(genpath(folder));

% generate truth signal
[f, w, v, p, Rcart, duration, dt] = generateMeasurements();

% run QEKF filter
[R, v_, p_] = LIEKF_run(f, w, p, Rcart, duration, dt);

% Convert to euler angles for visualization
Rcart_ = zeros(3, duration/dt);  % Initialize an array to hold the Euler angles
for i = 1:duration/dt
    Rcart_(:, i) = rotm2eul(R(:, :, i), 'ZYX');
end

figure(1);
rows = 3;
columns = 3;
i = 0;
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0.1, 0.1, 0.9, 0.9]);

% plot position
i = i + 1;
subplot(rows, columns, i);
plot(p(1, :), 'linewidth', 2)
hold on; grid on
plot(p_(1, :), 'linewidth', 2)
ylabel('$x$', 'fontsize', 14, 'Interpreter', 'latex')
legend({'Position'}, 'location', 'Best')
axis([0 length(f) -50 50]);
title('position')

% plot error in position
i = i + 1;
subplot(rows, columns, i);
plot(p(1, :)-p_(1, :), 'linewidth', 2)
hold on; grid on;
plot(p(2, :)-p_(2, :), 'linewidth', 2)
plot(p(3, :)-p_(3, :), 'linewidth', 2)
ylabel('$x$', 'fontsize', 14, 'Interpreter', 'latex')
legend({'Error in x','Error in y','Error in z'}, 'location', 'Best')
axis([0 length(f) -50 50]);
title('error in position')

% plot velocity
i = i + 1;
subplot(rows, columns, i);
plot(v(1, :), 'linewidth', 2)
hold on; grid on
plot(v_(1, :), 'linewidth', 2)
ylabel('$x$', 'fontsize', 14, 'Interpreter', 'latex')
legend({'Position'}, 'location', 'Best')
axis([0 length(f) -50 50]);
title('velocity')

% plot error in velocity
i = i + 1;
subplot(rows, columns, i);
plot(v(1, :)-v_(1, :), 'linewidth', 2)
hold on; grid on;
plot(v(2, :)-v_(2, :), 'linewidth', 2)
plot(v(3, :)-v_(3, :), 'linewidth', 2)
ylabel('$x$', 'fontsize', 14, 'Interpreter', 'latex')
legend({'Error in x','Error in y','Error in z'}, 'location', 'Best')
axis([0 length(f) -50 50]);
title('error in velocity')

% plot attitude
i = i + 1;
subplot(rows, columns, i);
plot(Rcart(1, :), 'linewidth', 2)
hold on; grid on
plot(Rcart_(1, :), 'linewidth', 2)
ylabel('$x$', 'fontsize', 14, 'Interpreter', 'latex')
legend({'Position'}, 'location', 'Best')
axis([0 length(f) -4 4]);
title('attitude (euler angles "x")')

% plot attitude
i = i + 1;
subplot(rows, columns, i);
plot(Rcart(2, :), 'linewidth', 2)
hold on; grid on
plot(Rcart_(2, :), 'linewidth', 2)
ylabel('$x$', 'fontsize', 14, 'Interpreter', 'latex')
legend({'Position'}, 'location', 'Best')
axis([0 length(f) -4 4]);
title('attitude (euler angles "y")')

% plot attitude
i = i + 1;
subplot(rows, columns, i);
plot(Rcart(3, :), 'linewidth', 2)
hold on; grid on
plot(Rcart_(3, :), 'linewidth', 2)
ylabel('$x$', 'fontsize', 14, 'Interpreter', 'latex')
legend({'Position'}, 'location', 'Best')
axis([0 length(f) -4 4]);
title('attitude (euler angles "z")')