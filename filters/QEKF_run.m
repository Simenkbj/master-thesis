% QEKF run test file
%
% Author: Simen Bjerkestrand <Simenkbj@stud.ntnu.no>
% Date: 2024-04-28

function [R, v, p, P] = QEKF_run(f, w, p_, Rcart, duration, dt)

    % initialize rotation
    eulerInitial = Rcart(:,1)';
    rotationInitial = eul2rotm(eulerInitial, 'XYZ')*eul2rotm([(10*pi/180) (120*pi/180) (20*pi/180)], "ZYX");
    R = zeros(3, 3, duration/dt);

    % initialize position
    positionInitial = p_(:,1);
    p = zeros(3, duration/dt);

    % initialize velocity
    velocityInitial = [0 0 0]';
    v = zeros(3, duration/dt);

    % initialize covariance
    P = zeros(9, 9, duration/dt);  

    % accelerometer white noise 51/56 NavL1
    velocity_random_walk = 0.2;
    sigma_w_acc = velocity_random_walk/60;

    % gyroscope white noise 46/56 NavL1
    w_ars = 0.15;
    sigma_w_ars = w_ars*pi/(60*180);

    % Measurement noise spectral density matrix
    sigma_eps_pos = 0.5;
    R = eye(3)*sigma_eps_pos^2;
    
    % initialize filter
    init.X = [rotationInitial, velocityInitial, positionInitial;
              zeros(2,3), eye(2)];
    init.P = eye(9);
    init.Qg = eye(3)*sigma_w_acc^2;
    init.Qa = eye(3)*sigma_w_ars^2;
    init.Qp = eye(3)*sigma_eps_pos^2;
    filter = QEKF(init);
    
    for i = 2:(duration/dt)
        % prediction
        filter.prediction(w(:, i), f(:, i), dt)

        % correction
        if mod(i,100) == 0
            filter.update(p_(:, i))
        end

        % save the state
        [R(:, :, i), v(:, i), p(:, i)] = filter.separate_state(filter.X);

        % save the covariance
        P(:, :, i) = filter.P;
    end
end