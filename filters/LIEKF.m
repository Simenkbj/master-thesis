% Left-Invariant Extended Kalman Filter for SE_2(3) based on code of Hao Zhou
%
% Author: Simen Bjerkestrand

classdef LIEKF < handle
    properties 
        X;     % State mean
        P;     % State covariance matrix
        
        Qg;    % Gyro covariance matrix
        Qa;    % Accel covariance matrix
        Qp;    % GPS position covariance matrix
        
        H;     % H matrix in update step
        b;     % b matrix in update step
        
        g;     % Gravity vector
        
        XCart; % State mean in cartesian frame: (r,p,y,px,py,pz,vx,vy,vz)
        PCart; % State covariance matrix in cartesian frame
        
        Quaternion = [1 0 0 0]; % quaternion describing the Earth relative to the sensor
        Beta = 2;               % Madgwick's algorithm gain
    end
    
    methods
        function obj = LIEKF(init)
            obj.X = init.X;
            obj.P = init.P;
            obj.Qg = init.Qg;
            obj.Qa = init.Qa;
            obj.Qp = init.Qp;
            obj.H = [zeros(3), zeros(3), eye(3)];
            obj.b = [zeros(4,1); 1];
            obj.g = [0; 0; 9.81];
        end
        
        function [R, v, p] = separate_state(~, X)
            % Separate state vector into components
            R = X(1:3, 1:3); % Orientation
            v = X(1:3, 4);   % Base Velocity
            p = X(1:3, 5);   % Base Position
        end
        
        function X = construct_state(~, R, v, p)
            % Construct matrix from separate states
            X = eye(5);
            X(1:3,1:5) = [R, v, p];
        end
        
        function A = skew(~, v)
            % Convert from vector to skew symmetric matrix
            A = [    0, -v(3),  v(2);
                  v(3),     0, -v(1);
                 -v(2),  v(1),    0];
        end
        
        function dX = exp(obj, v)
            % Exponential map of SE_2(3)
            Lg = zeros(5);
            Lg(1:3, :) = [obj.skew(v(1:3)), v(4:6), v(7:9)];
        	dX = expm(Lg);
        end
        
        function AdjX = Adjoint(obj, X)
            % Adjoint of SE_2(3)         
            [R, v, p] = obj.separate_state(X);
            RCell = repmat({R}, 1, 3); 
            AdjX = blkdiag(RCell{:});
            AdjX(4:6, 1:3) = obj.skew(v) * R;
            AdjX(7:9, 1:3) = obj.skew(p) * R;
        end
        
        function prediction(obj, w, f, dt)
            % Quaterion Extended Kalman Filter prediction step
            [R, v, p] = obj.separate_state(obj.X);
            
            % rotation matrix to quaterion
            qprev = rotm2quat(R);
            qprev = qprev/norm(qprev);
        
            alpha = w*dt;
            norm_alpha = norm(alpha);
            skew_alpha = skew(alpha);
        
            if norm_alpha ~= 0
                q = quatmultiply(qprev, [cos(norm_alpha/2);(sin(norm_alpha/2)/norm_alpha*alpha)]');
            else
                q = qprev;
            end
        
            % normalize quaternion
            q = q/norm(q);
        
            % quaternion to rotation matrix
            RPred = quat2rotm(q);
        
            % Calculate Average Rotation Matrix
            if norm_alpha>1.E-8
                Ravg = R * (eye(3) + (1 - cos(norm_alpha)) / norm_alpha^2 ...
                    * skew_alpha + (1 - sin(norm_alpha) / norm_alpha) / norm_alpha^2 ...
                    * skew_alpha * skew_alpha);
            else
                 Ravg = R;
            end

            % force rotation matrix on SO3
            Ravg = forceRotMat2SO3(Ravg);
        
            % prediction step
            a = Ravg*f+obj.g;
            vPred = v + a*dt;
            pPred = p + v*dt + 0.5*(a)*dt^2;
            
            % Construct predicted state
            obj.X = obj.construct_state(RPred, vPred, pPred);

            % Linearized continuous invariant error dynamics
            Ac = [-obj.skew(w),    zeros(3),       zeros(3);
                  -obj.skew(a),    -obj.skew(w),   zeros(3);
                  zeros(3),        eye(3),         -obj.skew(w)];
    
            % slide 51/59 NavL3
            G = [zeros(3) zeros(3);
                -eye(3) zeros(3);
                zeros(3) -eye(3)];
                
            % Continous covariance matrix
            Qc = blkdiag(obj.Qg, obj.Qa);

            % Discretized transition matrix
            [Phik, Qk] = calc_Phi_and_Qd_using_van_Loan(Ac, G, Qc, dt);

            % Predict Covariance
            obj.P = Phik * obj.P * Phik' + Qk;
        end
 
        function update(obj, Y)
            % Update state and covariance from a measurement
            % Compute Kalman gain L
            invX = obj.X \ eye(size(obj.X));
            S = obj.H * obj.P * obj.H' + obj.Qp;
            L = (obj.P * obj.H') / S;
            
            % Update state
            innovation = invX * Y - obj.b;
            innovation = innovation(1:3);
            delta = L * innovation;
            dX = obj.exp(delta);
            obj.X = obj.X * dX;
            
            % Update covariance
            I = eye(size(obj.P));
            % Joseph update form
            % obj.P = (I - L * obj.H) * obj.P * (I - L * obj.H)' + L * N * L';
            obj.P = obj.P - L * (obj.H * obj.P *obj.H' + obj.Qp) * L';
        end
    end % endmethods
end % classdef