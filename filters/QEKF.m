% Quaternion Extended Kalman Filter for fusing IMU and GPS
%
% Author: Simen Bjerkestrand <Simenkbj@stud.ntnu.no>
% Date:   2020-04-28

classdef QEKF < handle
    properties
        X;     % State mean
        P;     % State covariance matrix
        
        Qg;    % Gyro covariance matrix
        Qa;    % Accel covariance matrix
        Qp;    % GPS position covariance matrix
        
        H;     % H matrix in update step
        b;     % b matrix in update step
        
        g;     % Gravity vector
        
        XCart; % State mean in cartesian frame: (r,p,y,px,py,pz,vx,vy,vz)
        PCart; % State covariance matrix in cartesian frame
        
        Quaternion = [1 0 0 0]; % quaternion describing the Earth relative to the sensor
    end
    methods
        function obj = QEKF(init)
            obj.X = init.X;
            obj.P = init.P;
            obj.Qg = init.Qg;
            obj.Qa = init.Qa;
            obj.Qp = init.Qp;
            obj.H = [eye(3), zeros(3), zeros(3)];
            obj.g = [0; 0; 9.81];
        end
        
        function [R, v, p] = separate_state(~, X)
            % Separate state vector into components
            R = X(1:3, 1:3); % Orientation
            v = X(1:3, 4);   % Base Velocity
            p = X(1:3, 5);   % Base Position
        end
        
        function X = construct_state(~, R, v, p)
            % Construct matrix from separate states
            X = eye(5);
            X(1:3,1:5) = [R, v, p];
        end
        
        function A = skew(~, v)
            % Convert from vector to skew symmetric matrix
            A = [    0, -v(3),  v(2);
                  v(3),     0, -v(1);
                 -v(2),  v(1),    0];
        end
        
        function dX = exp(obj, v)
            % Exponential map of SE_2(3)
            Lg = zeros(5);
            Lg(1:3, :) = [obj.skew(v(1:3)), v(4:6), v(7:9)];
        	dX = expm(Lg);
        end
        
        function prediction(obj, w, f, dt)
            % Quaterion Extended Kalman Filter prediction step
            [R, v, p] = obj.separate_state(obj.X);
            
            % rotation matrix to quaterion
            qprev = rotm2quat(R);
            qprev = qprev/norm(qprev);
        
            alpha = w*dt;
            norm_alpha = norm(alpha);
            skew_alpha = skew(alpha);
        
            if norm_alpha ~= 0
                q = quatmultiply(qprev, [cos(norm_alpha/2);(sin(norm_alpha/2)/norm_alpha*alpha)]');
            else
                q = qprev;
            end
        
            % normalize quaternion
            q = q/norm(q);
        
            % quaternion to rotation matrix
            RPred = quat2rotm(q);
        
            if norm_alpha>1.E-8
                Ravg = R * (eye(3) + (1 - cos(norm_alpha)) / norm_alpha^2 ...
                    * skew_alpha + (1 - sin(norm_alpha) / norm_alpha) / norm_alpha^2 ...
                    * skew_alpha * skew_alpha);
            else
                 Ravg = R;
            end %if mag_alpha 
            
            % force rotation matrix on SO3
            Ravg = forceRotMat2SO3(Ravg);
        
            a = Ravg*f+obj.g;
            vPred = v + a*dt;
            pPred = p + v*dt + 0.5*(a)*dt^2;
            
            % Linearized continuous invariant error dynamics
            % Ac = [-obj.skew(w),     zeros(3),     zeros(3);
            %       -obj.skew(a), -obj.skew(w),     zeros(3);
            %           zeros(3),       eye(3), -obj.skew(w)];
            % slide 51/59 NavL3
            Ac = [zeros(3)    eye(3)    zeros(3);
                 zeros(3)    zeros(3)  -Ravg*skew(a);
                 zeros(3)    zeros(3)  -skew(w)];
            % Discrete-time state transition matrix \Phi = expm(Ac * dt)
            % Phik = eye(size(Ac)) + Ac*dt
            % Phik = expm(Ac * dt);

            % slide 51/59 NavL3
            G = [zeros(3) zeros(3);
                 -Ravg zeros(3);
                 zeros(3) -eye(3)];
            
            % Continous covariance matrix
            Qc = blkdiag(obj.Qg, obj.Qa);
            % Discretized covariance matrix
            % Qk = Phik * Qc * Phik' * dt;
            [Phik, Qk] = calc_Phi_and_Qd_using_van_Loan(Ac, G, Qc, dt);
            
            % Construct predicted state
            obj.X = obj.construct_state(RPred, vPred, pPred);
            % Predict Covariance
            obj.P = Phik * obj.P * Phik' + Qk;
        end
 
        function update(obj, z)
            % Update state and covariance from a measurement
            % Compute Kalman gain L
            
            S = obj.H * obj.P * obj.H' + obj.Qp;
            L = (obj.P * obj.H') / S;
            
            % Update state
            [R, v, p] = obj.separate_state(obj.X);
            delta = L*(z-p);
            
            % calculate new attitude
            q = rotm2quat(R);
            q = quatmultiply(q, [1;-0.5*delta(7:9)]');
            q = q/norm(q);
            RPred = quat2rotm(q);

            % calculate new velocity
            vPred = v + delta(4:6);

            % calculate new position
            pPred = p + delta(1:3);

            % insert them into the state representationf
            obj.X = obj.construct_state(RPred, vPred, pPred);

            % Update covariance
            I = eye(size(obj.P));
            % Joseph update form
            % obj.P = (I - L * obj.H) * obj.P * (I - L * obj.H)' + L * N * L';
            obj.P = obj.P - L * (obj.H * obj.P *obj.H' + obj.Qp) * L';
        end
    end % endmethods
end