function [r_i_ib,R_i_b,Euler_i_b,duration,dt] = generateProfile()

    % configuration
    dt = 0.01;
    duration = 50;
    
    % generate motion profile
    t = 0:dt:duration;
    speed = 1.5;
    
    R_i_b = cell(1, duration/dt+1, 1);
    Euler_i_b = zeros(3, duration/dt+1);
    r_i_ib = zeros(3, duration/dt);

    R0 = [1 0 0; 0 1 0; 0 0 1];

    time_start = (duration/dt)/5;
    vertical_offset = -1;
    for i = 0:duration/dt
        if i > time_start
        t2 = -speed*(i-time_start)*dt;
        r_i_ib(:,i) = [cos(t2*speed)+vertical_offset, cos(t2*2*speed)+vertical_offset, cos(t2*speed)+vertical_offset]';
        R_i_b{i+1} = forceRotMat2SO3(R0*[cos(t2) -sin(t2) 0;...
                                        sin(t2) cos(t2) 0;...
                                        0       0       1]*eul2rotm([0 0 cos(t2)], "ZYX"));
        else
            R_i_b{i+1} = R0*eye(3);
            r_i_ib(:,i+1) = zeros(3,1);
        end
        Euler_i_b(:,i+1) = rotm2eul(R_i_b{i+1}, "ZYX")';
    end
end

