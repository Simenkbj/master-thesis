function R_SO3 = forceRotMat2SO3( R, sensitivity)
%FORCEROTMATSO3 Function forcing the rotation matrix to be on SO3
% Input:
% Rotation matrix: R
% Sensitivity. Default is 0.5.

    if nargin == 1
        mu = 0.5;
    elseif nargin == 2
        mu = sensitivity;
    else
        error('Too few or too many inputs')
    end
    I3 = eye(3);

    r1 = R(:,1);
    r1_bar = r1/max( norm(r1), mu );

    r2 = R(:,2);
    r2_bar = ( (I3 - r1_bar*r1_bar')*r2 )/max( norm((I3 - r1_bar*r1_bar')*r2), mu );


    S_r1 = skew(r1_bar);
    R_SO3 = [r1_bar r2_bar S_r1*r2_bar ];

end
