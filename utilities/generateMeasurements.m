function [f, w, v_, p_, Rcart, duration, dt] = generateMeasurements()

    [p_, R_, Rcart, duration, dt] = generateProfile();

    % set constants
    g = [0 0 9.81]';

    % initialize measurement outputs
    f = zeros(3, duration/dt);
    w = zeros(3, duration/dt);
    v = zeros(3, duration/dt);


    % initialize velocity at 0
    vprev = [0 0 0]';
    
    for i = 2:(duration/dt)
        % position
        p = p_(:, i);
        pprev = p_(:, i-1);
    
        % orientation
        R = R_{i};
        Rprev = R_{i-1};
    
        % velocity
        v = (2/dt)*(p - pprev) - vprev;
        v_(:,i) = v;
    
        % acceleration
        a = (v - vprev)/dt;

        vprev = v;
    
        % specific force in inertial frame (J.4)
        f_i_ib = a - g;
    
        % coordinate transformation delta (J.6 inertial version)
        C_old_new = R'*Rprev;
    
        % attitude increment (J.7 & J.8)
        mu = acos(0.5*(C_old_new(1, 1) + C_old_new(2, 2) + C_old_new(3, 3) - 1.0));
    
        if mu >= 2e-5
            alpha = mu/(2*sin(mu))*[C_old_new(2, 3) - C_old_new(3, 2);
                                    C_old_new(3, 1) - C_old_new(1, 3);
                                    C_old_new(1, 2) - C_old_new(2, 1)];
        else
            alpha = 0.5*[C_old_new(2, 3) - C_old_new(3, 2);
                         C_old_new(3, 1) - C_old_new(1, 3);
                         C_old_new(1, 2) - C_old_new(2, 1)];
        end
    
        % angular rate (J.9)
        w(:,i) = alpha/dt;
    
        % average body to inertial frame transform
        alpha_mag = sqrt(alpha' * alpha);
        alpha_skew = skew(alpha);
        
        if alpha_mag>1.E-8
               Ravg = Rprev * (eye(3) + (1 - cos(alpha_mag)) /alpha_mag^2 ...
               * alpha_skew + (1 - sin(alpha_mag) / alpha_mag) / alpha_mag^2 ...
               * alpha_skew * alpha_skew);
    
               % implement forcing on so(3)
               Ravg = forceRotMat2SO3(Ravg);
    
        else
               Ravg = Rprev;
        end
    
        % specific force in body frame (J.5)
        f(:,i) = Ravg'*f_i_ib;
    end
end
