close all;

% Determine where your m-file's folder is.
folder = fileparts(which(mfilename)); 
% Add that folder plus all subfolders to the path.
addpath(genpath(folder));

% generate truth signal
[f, w, v, p, Rcart, duration, dt] = generateMeasurements();

% run QEKF filter
[R, v_, p_, P] = QEKF_run(f, w, p, Rcart, duration, dt);

% Convert to euler angles for visualization
Rcart_ = zeros(3, duration/dt);  % Initialize an array to hold the Euler angles
for i = 1:duration/dt
    Rcart_(:, i) = rotm2eul(R(:, :, i), 'ZYX');
end

figure(1);
rows = 3;
columns = 3;
i = 0;

% plot position
i = i + 1;
subplot(rows, columns, i);
plot(p(1, :), 'linewidth', 2)
hold on; grid on
plot(p_(1, :), 'linewidth', 2)
ylabel('$x$', 'fontsize', 14, 'Interpreter', 'latex')
legend({'Position'}, 'location', 'Best')
axis([0 length(f) -50 50]);
title('position')

% plot error in position
i = i + 1;
subplot(rows, columns, i);
P_reshaped = squeeze(P(1, 1, :));
error_in_position = p(1, :) - p_(1, :);
% Create vectors for shading the area
x = 1:length(error_in_position);
upper_bound = error_in_position + sqrt(P_reshaped');
lower_bound = error_in_position - sqrt(P_reshaped');
% Plot the error in position
plot(x, error_in_position, 'linewidth', 2);
hold on; grid on;
% Plot the shaded area for the covariance
fill([x fliplr(x)], [upper_bound fliplr(lower_bound)], 'r', 'FaceAlpha', 0.3, 'EdgeColor', 'none');
% Plot the covariance
plot(x, sqrt(P_reshaped), 'linewidth', 2);
ylabel('$x$', 'fontsize', 14, 'Interpreter', 'latex')
legend({'Error in position', 'Covariance'}, 'location', 'Best')
axis([0 length(f) -50 50]);
title('error in position')


% plot velocity
i = i + 1;
subplot(rows, columns, i);
plot(v(1, :), 'linewidth', 2)
hold on; grid on
plot(v_(1, :), 'linewidth', 2)
ylabel('$x$', 'fontsize', 14, 'Interpreter', 'latex')
legend({'Position'}, 'location', 'Best')
axis([0 length(f) -50 50]);
title('velocity')

% plot error in velocity
i = i + 1;
subplot(rows, columns, i);
plot(v(1, :)-v_(1, :), 'linewidth', 2)
hold on; grid on;
ylabel('$x$', 'fontsize', 14, 'Interpreter', 'latex')
legend({'Error in position'}, 'location', 'Best')
axis([0 length(f) -50 50]);
title('error in velocity')

% plot attitude
i = i + 1;
subplot(rows, columns, i);
plot(Rcart(1, :), 'linewidth', 2)
hold on; grid on
plot(Rcart_(1, :), 'linewidth', 2)
ylabel('$x$', 'fontsize', 14, 'Interpreter', 'latex')
legend({'Position'}, 'location', 'Best')
axis([0 length(f) -4 4]);
title('attitude (euler angles "x")')

% plot attitude
i = i + 1;
subplot(rows, columns, i);
plot(Rcart(2, :), 'linewidth', 2)
hold on; grid on
plot(Rcart_(2, :), 'linewidth', 2)
ylabel('$x$', 'fontsize', 14, 'Interpreter', 'latex')
legend({'Position'}, 'location', 'Best')
axis([0 length(f) -4 4]);
title('attitude (euler angles "y")')

% plot attitude
i = i + 1;
subplot(rows, columns, i);
plot(Rcart(3, :), 'linewidth', 2)
hold on; grid on
plot(Rcart_(3, :), 'linewidth', 2)
ylabel('$x$', 'fontsize', 14, 'Interpreter', 'latex')
legend({'Position'}, 'location', 'Best')
axis([0 length(f) -4 4]);
title('attitude (euler angles "z")')